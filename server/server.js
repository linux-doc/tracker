import { createServer } from 'https';
import { readFileSync } from 'fs';
import { WebSocketServer } from 'ws';
import { encode } from 'html-entities';

const server = createServer({
  cert: readFileSync('ssl/cert.pem'),
  key: readFileSync('ssl/key.pem')
});
const wss = new WebSocketServer({ server });

wss.on('connection', ws => {
  // console.log('Подключился клиент');
  // console.log(ws);
  console.log(wss.clients);

  ws.on('message', message => {
    message = encode(message.toString());
    console.log(ws._socket.remoteAddress, message.toString());
    console.log('received: %s', message);
    if (message === 'exit') {
      ws.close();
    }
    else {
      wss.clients.forEach(client => {
//        if (client.readyState === WebSocketServer.OPEN) {
          client.send(message);
//        }
      });
    }
  });
});

server.listen(3443);
