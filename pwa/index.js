if (
    window.location.protocol !== 'https:'
    && window.location.hostname !== 'localhost'
) {
    console.log('Для работы Service Worker нужно использовать протокол HTTPS или имя хоста должно быть localhost без HTTPS.');
} else if ('serviceWorker' in navigator) {
    console.log('Service Worker поддерживается.');
    navigator.serviceWorker
        .register('sw.js')
        .then(() => {
            console.log('Service Worker зарегистрирован.');
        })
} else {
    console.log('Service Worker не поддерживается.');
}

function showNotification() {
  Notification.requestPermission(function(result) {
    if (result === 'granted') {
      navigator.serviceWorker.ready.then(function(registration) {
        registration.showNotification('Vibration Sample', {
          body: 'Buzz! Buzz!',
          icon: '../images/touch/chrome-touch-icon-192x192.png',
          vibrate: [200, 100, 200, 100, 200, 100, 200],
          tag: 'vibration-sample'
        });
      });
    }
  });
}
