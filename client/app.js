let sendButton = document.getElementById('send');
let messageInput = document.getElementById('message');
let messagesDiv = document.getElementById('messages');

let statusDiv = document.getElementById('status');

let wss = new WebSocket("wss://universo.pro:3443");

document.getElementById('screen_width').innerHTML = screen.width;
document.getElementById('screen_height').innerHTML = screen.height;
document.getElementById('window_devicePixelRatio').innerHTML = window.devicePixelRatio;

document.getElementById('window_innerWidth').innerHTML = window.innerWidth;
document.getElementById('window_innerHeight').innerHTML = window.innerHeight;

document.getElementById('ppi').innerHTML = document.getElementById('div_ppi').clientWidth + ' x ' + document.getElementById('div_ppi').clientHeight;

wss.onopen = function () {
  statusDiv.style.backgroundColor = '#99ff99';
  statusDiv.innerHTML = 'Подключено';
};

wss.onclose = function () {
  statusDiv.style.backgroundColor = '#ff9999';
  statusDiv.innerHTML = 'Отключено';
};

wss.onmessage = response => {
  messagesDiv.innerHTML += response.data + "<br>\n";
  messagesDiv.scrollTo(0, messagesDiv.scrollHeight);
};

sendButton.addEventListener('click', event => {
  send();
});

messageInput.addEventListener('keydown', event => {
  const keyCode = event.which || event.keyCode;

  if (
      keyCode === 13
      && ! event.shiftKey
  ) {
    event.preventDefault();
    send();
  }
});

const send = () => {
  let message = messageInput.value.trim();
  messageInput.value = '';
  messageInput.focus();
  
  if (message == '') {
    return;
  }
  
  wss.send(message);
}
